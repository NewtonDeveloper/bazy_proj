#!/bin/python3
from datetime import *
from tkinter import *
from tkinter import ttk
import psycopg2
import psycopg2.extras
from log_in import *
from przeglad import *



class RejestracjaTab:
   """Klasa odpowiedzialna za zakladke Rejestracja"""
   def __init__(self):
        """Konstruktor tworzy zawartosc zakladki Rejestracja"""
        conn = psycopg2.connect("dbname={} host={} user={} password={}".format(db_name, db_host, db_un.get(), db_pw.get()),cursor_factory=psycopg2.extras.RealDictCursor)
        conn.close() 
        self.content = ttk.Frame(root)
        self.content.grid(column=0, row=0, sticky=(N, S, E, W))
        ########################################################## 
        #frame patient
        self.patient = ttk.Frame(self.content, padding=(3,3,12,12))

        self.patient['borderwidth'] = 2
        self.patient['relief'] = 'sunken'
        self.patient.grid(column=0, row=0, sticky=(N, S, E, W),
                             columnspan=30, rowspan=30)

        self.add_adrress_lbl = ttk.Label(self.patient, text="Dodaj Pacjenta:")
        self.add_adrress_lbl.grid(column=0, row=0, columnspan=30, sticky=(N, W))

        #house number
        self.house_lbl = ttk.Label(self.patient,
                                   text="numer_domu")
        self.house_lbl.grid(column=0, row=1, columnspan=1, sticky=(N, E))
        self.house_txt = StringVar()
        self.house_en = ttk.Entry(self.patient,textvariable=self.house_txt)
        self.house_en.grid(column=1, row=1, columnspan=1, sticky=(N, W))

        #street
        self.street_lbl = ttk.Label(self.patient,
                                    text="ulica")
        self.street_lbl.grid(column=0, row=2, columnspan=1, sticky=(N, E))
        self.street_txt = StringVar()
        self.street_en = ttk.Entry(self.patient,textvariable=self.street_txt)
        self.street_en.grid(column=1, row=2, columnspan=1, sticky=(N, W))
        #locality
        self.locality_lbl = ttk.Label(self.patient,
                                    text="miejscowosc")
        self.locality_lbl.grid(column=0, row=3, columnspan=1, sticky=(N, E))
        self.locality_txt = StringVar()
        self.locality_en = ttk.Entry(self.patient,textvariable=self.locality_txt)
        self.locality_en.grid(column=1, row=3, columnspan=1, sticky=(N, W))

        #post code
        self.post_lbl = ttk.Label(self.patient,
                                    text="kod_pocztowy")
        self.post_lbl.grid(column=0, row=4, columnspan=1, sticky=(N, E))
        self.post_txt = StringVar()
        self.post_en = ttk.Entry(self.patient,textvariable=self.post_txt)
        self.post_en.grid(column=1, row=4, columnspan=1, sticky=(N, W))

        # add button
        address_but = ttk.Button(self.patient, text="Dodaj Adres",command=self.add_address)
        address_but.grid(column=0, row=5, columnspan=1, sticky=(E,W,S))
        #address_id
        self.address_id_lbl = ttk.Label(self.patient,
                                    text="adres_id")
        self.address_id_lbl.grid(column=0, row=6, columnspan=1, sticky=(N, E))
        self.address_id_txt =StringVar()
        self.address_id_en=ttk.Entry(self.patient,textvariable=self.address_id_txt)
        self.address_id_en.grid(column=1, row=6, columnspan=1, sticky=(N, W))

       
        #firstname
        self.firstname_lbl = ttk.Label(self.patient,
                                    text="imie")
        self.firstname_lbl.grid(column=0, row=7, columnspan=1, sticky=(N, E))
        self.firstname_txt = ttk.Entry(self.patient)
        self.firstname_txt.grid(column=1, row=7, columnspan=1, sticky=(N, W))

        #lastname
        self.lastname_lbl = ttk.Label(self.patient,
                                       text="nazwisko")
        self.lastname_lbl.grid(column=0, row=8, columnspan=1, sticky=(N, E))
        self.lastname_txt = StringVar()
        self.lastname_en = ttk.Entry(self.patient,textvariable=self.lastname_txt)
        self.lastname_en.grid(column=1, row=8, columnspan=1, sticky=(N, W))
        #gender
        self.gender_lbl = ttk.Label(self.patient,
                                    text="płec")
        self.gender_lbl.grid(column=0, row=9, columnspan=1, sticky=(N, E))
        self.gendervar = StringVar()
        gender_choose = ttk.Combobox(self.patient,
                             textvariable=self.gendervar)
        gender_choose['values'] = ('K', 'M')
        gender_choose.grid(column=1,row=9,columnspan=1,
                        sticky=(N,E,W))

        #pesel
        self.pesel_lbl = ttk.Label(self.patient,
                                       text="pesel")
        self.pesel_lbl.grid(column=0, row=10, columnspan=1, sticky=(N, E))
        self.pesel_txt = StringVar()
        self.pesel_en = ttk.Entry(self.patient,textvariable=self.pesel_txt)
        self.pesel_en.grid(column=1, row=10, columnspan=1, sticky=(N, W))
 
        #birth date
        self.birth_lbl = ttk.Label(self.patient,
                                       text="data_narodzin")
        self.birth_lbl.grid(column=0, row=11, columnspan=1, sticky=(N, E))
        self.birth_lbl2 = ttk.Label(self.patient,
                                    text="yyyy-mm-dd")
        self.birth_lbl2.grid(column=2, row=11, columnspan=1, sticky=(N, W))
 
        self.birth_txt = StringVar()
        self.birh_en = ttk.Entry(self.patient,textvariable=self.birth_txt)
        self.birh_en.grid(column=1, row=11, columnspan=1, sticky=(N, W))
        # date_become_patient

        self.date_become_patient_txt = StringVar()
        self.date_become_patient_txt.set(str(datetime.now().date()))
 
        #phone
        self.phone_lbl = ttk.Label(self.patient,
                                       text="telefon")
        self.phone_lbl.grid(column=0, row=12, columnspan=1, sticky=(N, E))
        self.phone_txt = StringVar()
        self.phone_en = ttk.Entry(self.patient,textvariable=self.phone_txt)
        self.phone_en.grid(column=1, row=12, columnspan=1, sticky=(N, W))
        

        # add button
        patient_but = ttk.Button(self.patient,text="Dodaj",command=self.add_patient)
        patient_but.grid(column=0, row=13, columnspan=1, sticky=(E,W,S))
        ##########################################################
        #frame referral
        self.referral = ttk.Frame(self.content, padding=(3,3,12,12))
        self.referral['borderwidth'] = 2
        self.referral['relief'] = 'sunken'
        self.referral.grid(column=0, row=31, sticky=(N, S, E, W),
                             columnspan=30, rowspan=30)

        self.referral_lbl = ttk.Label(self.referral, text="Dodaj Skierowanie:")
        self.referral_lbl.grid(column=0, row=30, columnspan=30, sticky=(N, W))

        #code_icd10 
        self.code_lbl = ttk.Label(self.referral,
                                       text="kod_icd10")
        self.code_lbl.grid(column=0, row=31, columnspan=1, sticky=(N, E))
        self.code_txt = StringVar()
        self.code_en = ttk.Entry(self.referral,textvariable=self.code_txt)
        self.code_en.grid(column=1, row=31, columnspan=1, sticky=(N, W))
 
        #diagnosis 
        self.diagnosis_lbl = ttk.Label(self.referral,
                                       text="diagnoza")
        self.diagnosis_lbl.grid(column=0, row=32, columnspan=1, sticky=(N, E))
        self.diagnosis_txt = StringVar()
        self.diagnosis_en =ttk.Entry(self.referral,textvariable=self.diagnosis_txt)
        self.diagnosis_en.grid(column=1, row=32, columnspan=1, sticky=(N, W))
 
        #referral date 
        self.referral_date_lbl = ttk.Label(self.referral,
                                       text="data_skierowania")
        self.referral_date_lbl.grid(column=0, row=33, columnspan=1, sticky=(N, E))
        self.referral_date_lbl2 = ttk.Label(self.referral,
                                       text="yyyy-mm-dd")
        self.referral_date_lbl2.grid(column=2, row=33, columnspan=1, sticky=(N,W))
        
        self.referral_date_txt = StringVar()
        self.referral_date_en =ttk.Entry(self.referral,textvariable=self.referral_date_txt)
        self.referral_date_en.grid(column=1, row=33, columnspan=1, sticky=(N, W))

        # add button
        patient_but = ttk.Button(self.referral,
                                 text="Dodaj",command=self.add_referral)
        patient_but.grid(column=0, row=34, columnspan=1, sticky=(E,W,S))
        ##################################################### 
        #frame appointment
        self.appointment = ttk.Frame(self.content, padding=(3,3,12,12))
        self.appointment['borderwidth'] = 2
        self.appointment['relief'] = 'sunken'
        self.appointment.grid(column=30, row=0, sticky=(N, S, E, W),
                             columnspan=30, rowspan=30)

        self.referral_lbl = ttk.Label(self.appointment, text="Dodaj Wizyte:")
        self.referral_lbl.grid(column=30, row=0, columnspan=30, sticky=(N, W))

        #patient_id 
        self.patient_id_lbl = ttk.Label(self.appointment,
                                       text="pacjent_id")
        self.patient_id_lbl.grid(column=30, row=31, columnspan=1, sticky=(N, E))
        self.patient_id_txt = StringVar()
        self.patient_id_en = ttk.Entry(self.appointment,textvariable=self.patient_id_txt)
        self.patient_id_en.grid(column=31, row=31, columnspan=1, sticky=(N, W))

        #doctor_id 
        self.doctor_id_lbl = ttk.Label(self.appointment,
                                       text="doktor_id")
        self.doctor_id_lbl.grid(column=30, row=32, columnspan=1, sticky=(N, E))
        self.doctor_id_txt = StringVar()
        self.doctor_id_en =ttk.Entry(self.appointment,textvariable=self.doctor_id_txt)
        self.doctor_id_en.grid(column=31, row=32, columnspan=1, sticky=(N, W))

        #referral_id 
        self.referral_id_lbl = ttk.Label(self.appointment,
                                       text="skierowanie_id")
        self.referral_id_lbl.grid(column=30, row=33, columnspan=1, sticky=(N, E))
        self.referral_id_txt = StringVar()
        self.referral_id_en=ttk.Entry(self.appointment,textvariable=self.referral_id_txt)
        self.referral_id_en.grid(column=31, row=33, columnspan=1, sticky=(N, W))

        #visit_date 
        self.visit_date_lbl = ttk.Label(self.appointment,
                                       text="data_wizyty")
        self.visit_date_lbl.grid(column=30, row=34, columnspan=1, sticky=(N, E))
        self.visit_date_lbl2 = ttk.Label(self.appointment,
                                       text="yyyy-mm-dd")
        self.visit_date_lbl2.grid(column=32, row=34, columnspan=1, sticky=(N,W))
 
        self.visit_date_txt = StringVar()
        self.visit_date_en = ttk.Entry(self.appointment,textvariable=self.visit_date_txt)
        self.visit_date_en.grid(column=31, row=34, columnspan=1, sticky=(N, W))

        #start_time
        self.start_time_lbl = ttk.Label(self.appointment,
                                       text="poczatek_czas")
        self.start_time_lbl.grid(column=30, row=35, columnspan=1, sticky=(N, E))
        self.start_time_lbl2 = ttk.Label(self.appointment,
                                         text="hh:mm")
        self.start_time_lbl2.grid(column=32, row=35, columnspan=1, sticky=(N, E))
 
        self.start_time_txt = StringVar()
        self.start_time_en =ttk.Entry(self.appointment,textvariable=self.start_time_txt)
        self.start_time_en.grid(column=31, row=35, columnspan=1, sticky=(N, W))

        #end_time
        self.end_time_lbl = ttk.Label(self.appointment,
                                       text="koniec_czas")
        self.end_time_lbl.grid(column=30, row=36, columnspan=1, sticky=(N, E))
        self.end_time_lbl2 = ttk.Label(self.appointment,
                                       text="hh:mm")
        self.end_time_lbl2.grid(column=32, row=36, columnspan=1, sticky=(N, E))
 
        self.end_time_txt = StringVar()
        self.end_time_en =ttk.Entry(self.appointment,textvariable=self.end_time_txt)
        self.end_time_en.grid(column=31, row=36, columnspan=1, sticky=(N, W))

        #total_cost
        self.total_cost_lbl = ttk.Label(self.appointment,
                                       text="całkowity_koszt")
        self.total_cost_lbl.grid(column=30, row=37, columnspan=1, sticky=(N, E))
        self.total_cost_txt = StringVar()
        self.total_cost_en =ttk.Entry(self.appointment,textvariable=self.total_cost_txt)
        self.total_cost_en.grid(column=31, row=37, columnspan=1, sticky=(N, W))

        #rest_visit
        self.rest_visit_lbl = ttk.Label(self.appointment,
                                       text="pozostałe")
        self.rest_visit_lbl.grid(column=30, row=38, columnspan=1, sticky=(N, E))
        self.rest_visit_txt = StringVar()
        self.rest_visit_en =ttk.Entry(self.appointment,textvariable=self.rest_visit_txt)
        self.rest_visit_en.grid(column=31, row=38, columnspan=1, sticky=(N, W))

        # add button
        visit_but = ttk.Button(self.appointment,
                               text="Dodaj",command=self.add_visit)
        visit_but.grid(column=30, row=39, columnspan=1, sticky=(E,W,S))
 
        self.is_doctor()

        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        for num in range(0,100):
            self.content.rowconfigure(num, weight=1)

        for num in range(0,100):
            self.content.columnconfigure(num, weight=1)




   def clean(self):
        """Czysci zawartosc zakladki"""
        #self.frame.pack_forget()
        #self.frame.grid_forget()
        self.content.pack_forget()
        self.content.grid_forget()


   def add_address(self):
       """Metoda  wywoluje bazodanowa funkcje dodaj_adres()"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
       db_name, db_host, db_un.get(), db_pw.get()),
       cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       sql = "select przychodnia.dodaj_adres(%s,%s,%s,%s);"
       args = (self.house_txt.get(),self.street_txt.get(),self.locality_txt.get(),self.post_txt.get())
       cur.execute(sql, args)
       cur.execute("select currval('przychodnia.adresy_adres_id_seq');")
       currval=cur.fetchall().pop().get('currval')
       conn.commit()
       cur.close()
       conn.close()
       self.address_id_txt.set(str(currval))
       self.address_done_lbl = ttk.Label(self.patient,
                                    text="zrobione!!!")
       self.address_done_lbl.grid(column=1, row=5, columnspan=2, sticky=(N,W,E))
 
       
   def add_patient(self):
       """Metoda wywoluje bazodanowa funkcje dodaj_pacjenta()"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
       db_name, db_host, db_un.get(), db_pw.get()),
       cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       sql = "select przychodnia.dodaj_pacjenta(%s,%s,%s,%s,%s,%s,%s,%s);"
       args=(self.address_id_txt.get(),self.firstname_txt.get(),self.lastname_txt.get(),self.gendervar.get(),self.pesel_txt.get(),self.birth_txt.get(),self.date_become_patient_txt.get(),self.phone_txt.get())
       cur.execute(sql, args)
       cur.execute("select currval('przychodnia.pacjenci_pacjent_id_seq');")
       currval=cur.fetchall().pop().get('currval')
       conn.commit()
       cur.close()
       conn.close()
       self.patient_id_txt.set(str(currval))
       self.patient_done_lbl = ttk.Label(self.patient,
                                    text="zrobione!!!")
       self.patient_done_lbl.grid(column=1, row=13, columnspan=2, sticky=(N,W,E))

   def add_referral(self):
       """Metoda wywoluje funkcje dodaj skierowanie"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
       db_name, db_host, db_un.get(), db_pw.get()),
       cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       sql = "select przychodnia.dodaj_skierowanie(%s,%s,%s);"
       args =(self.code_txt.get(),self.diagnosis_txt.get(),self.referral_date_txt.get())
       cur.execute(sql, args)
       cur.execute("select currval('przychodnia.skierowania_skierowanie_id_seq');")
       currval=cur.fetchall().pop().get('currval')
       conn.commit()
       cur.close()
       conn.close()
       self.referral_id_txt.set(str(currval))
       self.referral_done_lbl = ttk.Label(self.referral,
                                    text="zrobione!!!")
       self.referral_done_lbl.grid(column=1, row=34, columnspan=2, sticky=(N,W,E))


   def is_doctor(self):
       """Metoda sprawdza czy obecny uzytkownik jest doktorem"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
           db_name, db_host, db_un.get(), db_pw.get()),
           cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       cur.execute('select current_user;')
       name=cur.fetchall()[0]['current_user'];
       sql ="select rolname from pg_user join pg_auth_members on (pg_user.usesysid=pg_auth_members.member) join pg_roles on (pg_roles.oid=pg_auth_members.roleid) where pg_user.usename=%s";
       args=(name,)
       cur.execute(sql,args)
       if cur.fetchall()[0]['rolname'] == 'lekarz'\
          or name == 'lekarz' or name == 'admin':
            return True
       self.total_cost_en.config(state=DISABLED)
       return False



   def add_visit(self):
       """Metoda wywoluje funkcje bazodanowa dodaj_wizyte()"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
       db_name, db_host, db_un.get(), db_pw.get()),
       cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       sql = "select przychodnia.dodaj_wizyte(%s,%s,%s,%s,%s,%s,%s);"
       args=(self.patient_id_txt.get(),self.doctor_id_txt.get(),self.referral_id_txt.get(),self.visit_date_txt.get(),self.start_time_txt.get(),self.end_time_txt.get(),self.rest_visit_txt.get())
       cur.execute(sql, args)
       cur.execute("select currval('przychodnia.wizyty_wizyta_id_seq');")
       currval=cur.fetchall().pop().get('currval')
       conn.commit()
       cur.close()
       conn.close()
       self.visit_done_lbl = ttk.Label(self.appointment,
                                    text="zrobione!!!")
       self.visit_done_lbl.grid(column=31, row=39, columnspan=2, sticky=(N,W,E))



