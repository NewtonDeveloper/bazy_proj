#!/bin/python3
from datetime import *
from tkinter import *
from tkinter import ttk
import psycopg2
import psycopg2.extras
from log_in import *
from przeglad import *



class ProceduraTab:
   """Klasa odpowiedzialna za zakladke Dodaj Procedure"""
   def __init__(self):
        """Konstrukotr tworzy zawartość zakladki Dodaj Procedure"""
        conn = psycopg2.connect("dbname={} host={} user={} password={}".format(db_name, db_host, db_un.get(), db_pw.get()),cursor_factory=psycopg2.extras.RealDictCursor)
        conn.close() 
        self.content = ttk.Frame(root)
        self.content.grid(column=0, row=0, sticky=(N, S, E, W))
        ########################################################## 
        #frame patient
        self.patient = ttk.Frame(self.content, padding=(3,3,12,12))

        self.patient['borderwidth'] = 2
        self.patient['relief'] = 'sunken'
        self.patient.grid(column=0, row=0, sticky=(N, S, E, W),
                             columnspan=30, rowspan=30)

        self.add_adrress_lbl = ttk.Label(self.patient, text="Dodaj Procedure:")
        self.add_adrress_lbl.grid(column=15, row=0, columnspan=1, sticky=(N, W))

        # visit_id
        self.visit_id_lbl = ttk.Label(self.patient,
                                   text="wizyta_id")
        self.visit_id_lbl.grid(column=0, row=1, columnspan=1, sticky=(N, E))
        self.visit_id_txt = StringVar()
        self.visit_id_en = ttk.Entry(self.patient,textvariable=self.visit_id_txt)
        self.visit_id_en.grid(column=1, row=1, columnspan=1, sticky=(N, W))

        # procedure_id
        self.procedure_id_lbl = ttk.Label(self.patient,
                                    text="procedure_id")
        self.procedure_id_lbl.grid(column=0, row=2, columnspan=1, sticky=(N, E))
        self.procedure_id_txt = StringVar()
        self.procedure_id_en =ttk.Entry(self.patient,textvariable=self.procedure_id_txt)
        self.procedure_id_en.grid(column=1, row=2, columnspan=1, sticky=(N, W))

        #quantity
        self.quantity_lbl = ttk.Label(self.patient,
                                    text="ilosc")
        self.quantity_lbl.grid(column=0, row=3, columnspan=1, sticky=(N, E))
        self.quantity_txt = StringVar()
        self.quantity_en = ttk.Entry(self.patient,textvariable=self.quantity_txt)
        self.quantity_en.grid(column=1, row=3, columnspan=1, sticky=(N, W))

        # add button
        procedure_but =ttk.Button(self.patient,text="Dodaj",command=self.add_procedure)
        procedure_but.grid(column=0, row=4, columnspan=1, sticky=(E,W,S))
        #frame scrolled_window
        self.scrolled_box = self.scrolled_window()
        self.show_procedres()

        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        for num in range(0,100):
            self.content.rowconfigure(num, weight=1)

        for num in range(0,100):
            self.content.columnconfigure(num, weight=1)




   def clean(self):
        """Metoda czysci zawartość zakladki"""
        #self.frame.pack_forget()
        #self.frame.grid_forget()
        self.content.pack_forget()
        self.content.grid_forget()
        self.content.destroy()
        self.content.destroy()



   def add_procedure(self):
       """Wykonanie funkcji bazy danych dodaj_procedure()"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
       db_name, db_host, db_un.get(), db_pw.get()),
       cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       sql = "select przychodnia.dodaj_procedure(%s,%s,%s);"
       args = (self.visit_id_txt.get(),
               self.procedure_id_txt.get(),self.quantity_txt.get())
       cur.execute(sql, args)
       conn.commit()
       cur.close()
       conn.close()
       self.procedure_done_lbl = ttk.Label(self.patient,
                                    text="zrobione!!!")
       self.procedure_done_lbl.grid(column=1, row=4, columnspan=2, sticky=(N,W,E))




   def scrolled_window(self):
        """Dodanie okienka z suwaczkami"""
        l = Listbox(self.content)
        l.grid(column=30, row=0, sticky=(N,W,E,S), columnspan=60,rowspan=30)
        s = ttk.Scrollbar(self.content, orient=VERTICAL,
                                        command=l.yview)
        s.grid(column=91, row=0, sticky=(N,S), rowspan=30)
        h = ttk.Scrollbar(self.content, orient=HORIZONTAL,
                                            command=l.xview)
        h.grid(column=31, row=30, sticky=(W,E), columnspan=60)
        l['yscrollcommand'] = s.set
        l['xscrollcommand'] = h.set
        ttk.Sizegrip().grid(column=30, row=60, sticky=(S,E))
        #for i in range(1,101):
        #       l.insert('end', 'Line %d of 100' % i)
        return l

   def show_procedres(self):
       """Metoda wypisuje dostepne procedury medyczne"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
           db_name, db_host, db_un.get(), db_pw.get()),
           cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       sql = "select * from przychodnia.Procedury_Medyczne;"
       cur.execute(sql)
      #print(cur.fetchall())
       self.scrolled_box.delete(0,END)
       for row in cur.fetchall():
           for pair in row.items():
               self.scrolled_box.insert('end',str(pair[0]) + ': '                                       + str(pair[1]))
           self.scrolled_box.insert('end','\n')
       cur.close()
       conn.close()


