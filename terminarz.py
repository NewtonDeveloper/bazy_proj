#!/bin/python3
from datetime import *
import datetime
from tkcalendar import Calendar, DateEntry
from tkinter import *
from tkinter import ttk
import psycopg2
import psycopg2.extras
from log_in import *
from przeglad import *



class TerminarzTab:
   """Klasa odpowiedzialna za zakladke Terminarz"""
   def __init__(self):
        """Konstruktor tworzy zawartosc zakladki Terminarz"""
        conn = psycopg2.connect("dbname={} host={} user={} password={}".format(db_name, db_host, db_un.get(), db_pw.get()),cursor_factory=psycopg2.extras.RealDictCursor)
        conn.close() 
        self.content = ttk.Frame(root, padding=(3,3,12,12))
        self.content.grid(column=0, row=0, sticky=(N, S, E, W))
        self.scrolled_window()
        self.choose_doctors()


        self.but_cal=ttk.Button(self.content, text='Kalendarz', command=self.calendar_window)
        self.but_cal.grid(column=82, row=10, columnspan=2)
        
        date_choose_lbl = ttk.Label(self.content,
                                    text="Wybierz Datę")
        date_choose_lbl.grid(column=82, row=8,
                             columnspan=2, sticky=(N, W))
 
        self.date_txt = StringVar()
        self.date_txt.set('2018-12-22')
        self.date_en = ttk.Entry(self.content,
                             textvariable=self.date_txt)
        self.date_en.grid(column=82,row=11,columnspan=1,
                        sticky=(N,E,W))

        self.show_day_visits()
        root.rowconfigure(0, weight=1)
        root.columnconfigure(0, weight=1)
 
        for num in range(0,100):
            self.content.rowconfigure(num, weight=1)
            self.content.columnconfigure(num, weight=1)

   def scrolled_window(self):
        """Dodaje okienko z suwaczkami"""
        self.scrolled_box_lbl = ttk.Label(self.content,
                                    text="Lista pacjentów umówionych na wizytę (pierwsza kolumna to wizyta_id):")
        self.scrolled_box_lbl.grid(column=0, row=0,columnspan=20,
                                  sticky=(N,W))
 
        self.scrolled_box = Listbox(self.content)
        self.scrolled_box.grid(column=0, row=1, sticky=(N,W,E,S), columnspan=80,rowspan=90)
        s = ttk.Scrollbar(self.content, orient=VERTICAL,
                                        command=self.scrolled_box.yview)
        s.grid(column=81, row=1, sticky=(N,S,W), rowspan=90)
        h = ttk.Scrollbar(self.content, orient=HORIZONTAL,
                                            command=self.scrolled_box.xview)
        h.grid(column=0, row=91, sticky=(N,W,E), columnspan=80)
        self.scrolled_box['yscrollcommand'] = s.set
        self.scrolled_box['xscrollcommand'] = h.set
        ttk.Sizegrip().grid(column=81, row=90, sticky=(S,E))
#        for i in range(1,101):
#               l.insert('end', 'Line %d of 100' % i)

   def choose_doctors(self):
        """Wyswiela dostepnych doktorow jako liste do wyboru"""
        doctor_choose_lbl = ttk.Label(self.content, text="Wybierz Lekarza")
        doctor_choose_lbl.grid(column=82, row=0, columnspan=2, sticky=(N, W))
 
        self.doctor_txt = StringVar()
        self.doctors_box = ttk.Combobox(self.content,
                             textvariable=self.doctor_txt)
        doctors_list=[] 

        conn = psycopg2.connect("dbname={} host={} user={} password={}".format(db_name, db_host, db_un.get(), db_pw.get()),cursor_factory=psycopg2.extras.RealDictCursor)
        cur = conn.cursor()
        sql = "select nazwa_doktora from przychodnia.Doktorzy;"
        cur.execute(sql)
        #print(cur.fetchall())
        self.scrolled_box.delete(0,END)
        for row in cur.fetchall():
           for pair in row.items():
                doctors_list.append(str(pair[1]))
        cur.close()
        conn.close()
        self.doctors_box['values'] = tuple(doctors_list)
        self.doctors_box.grid(column=82,row=1,columnspan=1,
                        sticky=(N,E,W))
        self.doctors_box.bind('<<ComboboxSelected>>', self.show_day_visits)



   def show_day_visits(self, *args):
       """Wyswietla wynik umowionych wizyt w ciagu wybrango dnia dla danego doktora"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
           db_name, db_host, db_un.get(), db_pw.get()),
           cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       sql = "select w.wizyta_id, p.imie, p.nazwisko,\
               w.poczatek, w.koniec  from\
               przychodnia.Wizyty w join przychodnia.Pacjenci p\
               on w.pacjent_id=p.pacjent_id join przychodnia.\
               Doktorzy d on d.doktor_id=w.doktor_id\
               where w.data_wizyty=%s and d.nazwa_doktora=%s"
       args=(self.date_txt.get(),self.doctor_txt.get())
       cur.execute(sql,args)
       result =cur.fetchall()
       result.sort(key=(lambda listdict: listdict['poczatek']))
       #print(cur.fetchall())
       self.scrolled_box.delete(0,END)

       for row in result:
           self.scrolled_box.insert('end',str(row['wizyta_id'])
                                     +' '+row['imie']+' '
                                     +row['nazwisko']+' '
                                     +str(row['poczatek'])+ '-'
                                     +str(row['koniec']))
       cur.close()
       conn.close()

   def calendar_window(self):
        """Tworzy okno kalendarza"""
        top = Toplevel(root)

        self.cal = Calendar(top, font="Arial 14", selectmode='day', locale='en_US',
                   cursor="hand1", year=2018, month=12, day=22)

        self.cal.pack(fill="both", expand=True)
        ttk.Button(top, text="ok", command=self.print_sel).pack()


   def print_sel(self):
        """Metoda zatwierdza wybrany dzien z kalendarza"""
        self.date_txt.set(self.cal.selection_get())
        self.show_day_visits()
        print(self.cal.selection_get())



   def clean(self):
        """Czysci zawartosc zakladki"""
        #self.frame.pack_forget()
        #self.frame.grid_forget()
        self.content.grid_forget()
        self.content.destroy()




