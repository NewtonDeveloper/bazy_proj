#!/bin/python3
from tkinter import *
from tkinter import ttk

import psycopg2
import psycopg2.extras


root= Tk()
db_host = 'localhost'
db_un = None
db_pw = None
db_port = '5432'
db_name = 'project'
forward = False

class LoginWindow:
    """Klasa odpowiedzialna za zalogowanie sie do systemu"""
    def log_in(self, *args):
          """Metoda wywolywana za kazda proba zalogowani"""
          try:
                conn = psycopg2.connect("dbname={} host={}\
                                        user={} password={}"\
                        .format(db_name, db_host, db_un.get(),\
                        db_pw.get()),cursor_factory\
                        =psycopg2.extras.RealDictCursor)
                conn.close()
          except psycopg2.OperationalError as e:
                print(e)
                self.info.set('Nieprawidlowy uzytkownik\nlub haslo!')
                db_un.set('')
                db_pw.set('')
                return
          root.destroy()
          global forward
          forward=True

    def window_center(self):
        """Ustawienie okna na srodku"""
        root.update_idletasks()
        width = root.winfo_width()
        height = root.winfo_height()
        x = (root.winfo_screenwidth() // 2) - (width // 2)
        y = (root.winfo_screenheight() // 2) - (height // 2)
        root.geometry('{}x{}+{}+{}'.format(270,
        350,x, y))

    def __init__(self):
        """Konstruktor tworzy okno logowania"""
        global db_un
        global db_pw
        db_un = StringVar()
        db_pw = StringVar()
        self.info = StringVar()
        self.window_center()
        root.title("Okno logowania")
        mainframe = ttk.Frame(root, padding="3 3 12 12")
        mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)

        user_entry = ttk.Entry(mainframe, width=18, textvariable=db_un)
        user_entry.grid(column=2, row=1, sticky=(W,E))

        password_entry = ttk.Entry(mainframe, width=18, textvariable=db_pw)
        password_entry.configure(show="*")
        password_entry.grid(column=2, row=2, sticky=(W,E))

        ttk.Label(mainframe, textvariable=self.info).grid(column=2,row=3,sticky=(W,E))
        ttk.Button(mainframe, text="Zaloguj",command=self.log_in).grid(column=2,row=4,sticky=W)

        ttk.Label(mainframe, text="uzytkownik:").grid(column=1, row=1, sticky=W)
        ttk.Label(mainframe, text="haslo:").grid(column=1, row=2,sticky=E)

        self.logo_postgres = PhotoImage(file="PostgreSQL.png")
        self.logo_postgres_lbl=Label(mainframe,image=self.logo_postgres)
        self.logo_postgres_lbl.grid(row=0,column=0,rowspan=1,
                                    columnspan=3,sticky=(E,W,S))

        for child in mainframe.winfo_children():
            child.grid_configure(padx=5, pady=5)

        user_entry.focus()
        root.bind('<Return>', self.log_in)

loginwindow = LoginWindow()
root.mainloop()
