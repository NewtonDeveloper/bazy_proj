#!/bin/python3
from tkinter import *
from tkinter import ttk
import psycopg2
import psycopg2.extras
from przeglad import *
from procedura import *
from recepta import *
from terminarz import *
from rejestracja import *



class MenuWindow:
   """Klasa generuje panel kontrolny. Jest odpowiedzialna za
    przełaczanie pomiedzy zakladkammi"""
   def add_menubar(self):
        """Metoda dodaje zakladki panelu kontrolnego"""
        menubar = Menu(root)
        menu_visit = Menu(menubar)
        menubar.add_cascade(menu=menu_visit, label='Wizyta')
        menubar.add_command(label='Rejestracja',
                            command=self.register_tab)
        menubar.add_command(label='Przeglad',
                           command=self.przeglad_tab)
        menubar.add_command(label='Terminarz',
                           command=self.terminarz_tab)


        menu_visit.add_command(label='Dodaj Procedure',
                              command=self.procedure_tab)
        menu_visit.add_command(label='Dodaj Recepte',
                              command=self.receipt_tab)
        root.config(menu=menubar)

   def maximize_window(self):
        """Metoda robi tak zeby okno aplikacji bylo na caly ekran"""
        w, h = root.winfo_screenwidth(), root.winfo_screenheight()
        root.geometry("%dx%d+0+0" % (w, h))

   def clean_frame(self):
       """Metoda czysci zawartosc aplikacji"""
       if self.once == 0:
            self.content.pack_forget()
            self.content.grid_forget()
            self.content.destroy()
            self.content.destroy()
            self.once = 1
       print("clean_frame()")
       if self.przegladtab is not None:
            print("clean przeglad")
            self.przegladtab.clean()
            self.przegladtab = None
       if self.rejestracjatab is not None:
            print("clean tab")
            self.rejestracjatab.clean()
            self.rejestracjatab = None
       if self.proceduratab is not None:
            print("clean tab")
            self.proceduratab.clean()
            self.proceduratab = None
       if self.receptatab is not None:
            print("clean tab")
            self.receptatab.clean()
            self.receptatab = None
       if self.terminarztab is not None:
            print("clean tab")
            self.terminarztab.clean()
            self.terminarztab = None


   def register_tab(self):
        """Tworzenie zawartości zakładki rejestracja"""
        self.clean_frame()
        try:
            self.rejestracjatab = RejestracjaTab()
        except psycopg2.OperationalError as e:
            print(e)
            self.reset()
        except psycopg2.ProgrammingError as e:
            print(e)
            self.reset()


   def przeglad_tab(self):
        """Tworzenie zawartosci zakładki przeglad"""
        self.clean_frame()
        try:
            self.przegladtab = PrzegladTab()
        except psycopg2.OperationalError:
            print(e)
            self.reset()
        except psycopg2.ProgrammingError as e:
            print(e)
            self.reset()


   def terminarz_tab(self):
        """Tworzenie zawartosci zakladki terminarz"""
        self.clean_frame()
        try:
            self.terminarztab = TerminarzTab()
        except psycopg2.OperationalError:
            print(e)
            self.reset()
        except psycopg2.ProgrammingError as e:
            print(e)
            self.reset()


   def procedure_tab(self):
        """Tworzenie zawartoscci zakładki procedura"""
        self.clean_frame()
        try:
            self.proceduratab = ProceduraTab()
        except psycopg2.OperationalError:
            print(e)
            self.reset()
        except psycopg2.ProgrammingError as e:
            print(e)
            self.reset()


   def receipt_tab(self):
        """Tworzenie zawartosi zakladki recepta"""
        self.clean_frame()
        try:
            self.receptatab = ReceptaTab()
        except psycopg2.OperationalError:
            print(e)
            self.reset()
        except psycopg2.ProgrammingError as e:
            print(e)
            self.reset()

        
   def reset(self):
        """Metoda czysci zawrtosc aplikacji"""
        for widget in root.winfo_children():
             widget.destroy()
        self.create_frame()


   def __init__(self):
        """Konstruktor tworzy obiekt"""
        root.title('System zarządzania przychodnią specjalistyczną')
        self.przegladtab = None
        self.rejestracjatab = None
        self.proceduratab = None
        self.receptatab = None
        self.terminarztab = None
        self.once=0
        root.option_add('*tearOff', FALSE)
        self.maximize_window()
 
        self.create_frame()

   def create_frame(self):
        """Tworzy pustą zawartość niewidocznej zakładki menu"""
        self.add_menubar()
        self.content = ttk.Frame(root, padding=(3,3,12,12))
        self.content.grid(row=0, column=0)

        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        for num in range(0,100):
            self.content.columnconfigure(num, weight=1)

        for num in range(0,100):
            self.content.rowconfigure(num, weight=1)



if __name__ == "__main__":
    pass
else:
    menuwindow = MenuWindow()
    root.mainloop()
