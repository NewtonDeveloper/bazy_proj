#!/bin/python3
from datetime import *
from tkinter import *
from tkinter import ttk
import psycopg2
import psycopg2.extras
from log_in import *


root = Tk()

class PrzegladTab:
   """Klasa odpowiedzialna za zakladke Przeglad"""
   def __init__(self):
        """Konstruktor tworzy zawartosc zakladki Przeglad"""
        conn = psycopg2.connect("dbname={} host={} user={} password={}".format(db_name, db_host, db_un.get(), db_pw.get()),cursor_factory=psycopg2.extras.RealDictCursor)
        conn.close() 
        self.content = ttk.Frame(root, padding=(3,3,12,12))
        self.content.grid(column=0, row=0, sticky=(N, S, E, W))
#        self.frame = ttk.Frame(self.content, borderwidth=5,
#                        relief="sunken", width=200,height=100)
#        self.frame.grid(column=0, row=0, columnspan=3, rowspan=26, sticky=(N, S, E, W))
           
        self.scrolled_window()
        table_choice = ttk.Label(self.content, text="Wybierz")
        table_choice.grid(column=82, row=0, columnspan=1, sticky=(N, W))
        self.tablevar = StringVar()
        table = ttk.Combobox(self.content,
                             textvariable=self.tablevar)
        table['values'] = ('Pacjenci', 'Recepcjonisci',
                           'Doktorzy','Skierowania','Leki',
                           'Procedury_Medyczne','Autoryzacja',
                          'Adresy','Wizyty', 'Wizyta_Recepta'
                          ,'Wizyta_Procedura')
        table.grid(column=83,row=1,columnspan=1,
                        sticky=(N,E,W))
        try:
            table.bind('<<ComboboxSelected>>', self.show_table)
        except psycopg2.OperationalError as e:
            print(e)
        except psycopg2.ProgrammingError as e:
            print(e)

        self.record_count_txt = StringVar()

        count_lbl = ttk.Label(self.content,
                    textvariable=self.record_count_txt)
        count_lbl.grid(column=82, row=2, columnspan=2, sticky=(N, W))

        self.gen_report_lbl = ttk.Label(self.content, text="Generuj raport CSV:")
        self.gen_report_lbl.grid(column=82, row=50, columnspan=2, sticky=(N, W))
        self.from_lbl = ttk.Label(self.content, text="od:")
        self.from_lbl.grid(column=82, row=51, columnspan=1, sticky=(N, W))
        self.to_lbl = ttk.Label(self.content, text="do:")
        self.to_lbl.grid(column=82, row=52, columnspan=1, sticky=(N, W))
 

        self.date_from_txt = StringVar()
        self.date_from_txt.set('2018-12-01')
        self.date_from_en = ttk.Entry(self.content,
                             textvariable=self.date_from_txt)
        self.date_from_en.grid(column=83,row=51,columnspan=1,
                        sticky=(N,W))

        self.date_to_txt = StringVar()
        self.date_to_txt.set('2018-12-31')
        self.date_to_en = ttk.Entry(self.content,
                             textvariable=self.date_to_txt)
        self.date_to_en.grid(column=83,row=52,columnspan=1,
                        sticky=(N,W))
 
        self.but_referral=ttk.Button(self.content, 
                        text='Raport Skierowania', 
                        command=self.generate_report_referral)
        self.but_referral.grid(column=83, row=53, columnspan=1, sticky=(N,W))
 
        self.but_visit=ttk.Button(self.content,text='Raport Wizyty',command=self.generate_report_visits)
        self.but_visit.grid(column=83, row=54, columnspan=1,sticky=(N,W))
  
        self.but_patient=ttk.Button(self.content, text='Raport Pacjenci',command=self.generate_report_patient)
        self.but_patient.grid(column=83, row=55, columnspan=1,sticky=(N,W))
 

        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)

        for num in range(0,100):
            self.content.rowconfigure(num, weight=1)

        for num in range(0,100):
            self.content.columnconfigure(num, weight=1)


   def clean(self):
        """Czysci zawartosc zakladki"""
        self.content.pack_forget()
        self.content.grid_forget()
        self.content.destroy()


   def show_table(self, *args):
       """Metoda wyswietla zawartosc wybranej tabeli z listy"""
       print(self.tablevar.get())
       if self.tablevar.get()== 'Wizyty' and self.is_doctor()==False: 
           self.show_view()
           return
       self.scrolled_box.delete(0,END)
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
           db_name, db_host, db_un.get(), db_pw.get()),
           cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       sql = 'select * from przychodnia.'+self.tablevar.get()+';'
       cur.execute(sql)
      #print(cur.fetchall())
       for row in cur.fetchall():
           for pair in row.items():
               self.scrolled_box.insert('end',str(pair[0]) + ': '                                       + str(pair[1]))
           self.scrolled_box.insert('end','\n')

       cur.execute('select count(*) from\
                   przychodnia.'+self.tablevar.get()+';')
       self.record_count_txt.set("Liczba rekordów bazy:"
                              +str(cur.fetchall()[0]['count']))
       cur.close()
       conn.close()

   def show_view(self):
       """W przypadku wyswietlenia tabeli Wizyty wyswietlany jest widok zamiast"""
       self.scrolled_box.delete(0,END)
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
           db_name, db_host, db_un.get(), db_pw.get()),
           cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       sql ='select * from przychodnia.recepcjonista_pokaz_wizyty;'
       cur.execute(sql)
      #print(cur.fetchall())
       for row in cur.fetchall():
           for pair in row.items():
               self.scrolled_box.insert('end',str(pair[0]) + ': '                                       + str(pair[1]))
           self.scrolled_box.insert('end','\n')

       cur.execute('select count(*) from\
                   przychodnia.'+self.tablevar.get()+';')
       self.record_count_txt.set("Liczba rekordów bazy:"
                              +str(cur.fetchall()[0]['count']))
       cur.close()
       conn.close()


   def is_doctor(self):
       """Metoda sprawdza czy zalogowany uzytkownik jest doktorem"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
           db_name, db_host, db_un.get(), db_pw.get()),
           cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()
       cur.execute('select current_user;')
       name=cur.fetchall()[0]['current_user'];
       sql ="select rolname from pg_user join pg_auth_members on (pg_user.usesysid=pg_auth_members.member) join pg_roles on (pg_roles.oid=pg_auth_members.roleid) where pg_user.usename=%s";
       args=(name,)
       cur.execute(sql,args)
       if cur.fetchall()[0]['rolname'] == 'lekarz'\
          or name == 'lekarz' or name == 'admin':
            return True
       return False

   def scrolled_window(self):
        """Dodaje okienko z suwaczkami"""
        self.scrolled_box_lbl = ttk.Label(self.content,
                                          text="Podgląd tabel bazy PostgresSQL:")
        self.scrolled_box_lbl.grid(column=0, row=0,columnspan=20,
                                  sticky=(N,W))
 
        self.scrolled_box = Listbox(self.content)
        self.scrolled_box.grid(column=0, row=1, sticky=(N,W,E,S), columnspan=80,rowspan=90)
        s = ttk.Scrollbar(self.content, orient=VERTICAL,
                                        command=self.scrolled_box.yview)
        s.grid(column=81, row=1, sticky=(N,S,W), rowspan=90)
        h = ttk.Scrollbar(self.content, orient=HORIZONTAL,
                                            command=self.scrolled_box.xview)
        h.grid(column=0, row=91, sticky=(N,W,E), columnspan=80)
        self.scrolled_box['yscrollcommand'] = s.set
        self.scrolled_box['xscrollcommand'] = h.set
        ttk.Sizegrip().grid(column=81, row=90, sticky=(S,E))
#        for i in range(1,101):
#               l.insert('end', 'Line %d of 100' % i)

   def generate_report_patient(self):
       """Metoda tworzy raport pacjenta"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
           db_name, db_host, db_un.get(), db_pw.get()),
           cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()

       args=(self.date_from_txt.get(), self.date_to_txt.get())
       sql = """SELECT
plec "pacjenci / miesiac"
,extract(month FROM data_stania_sie_pacjentem) AS miesiac
, count(1) as ilosc
FROM
przychodnia.Pacjenci
WHERE
data_stania_sie_pacjentem BETWEEN '%s'
AND '%s'
GROUP BY
1
, 2
ORDER BY
1
"""%args
        
       outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(sql)

       with open('raport_pacjenci.csv', 'w') as f:
                cur.copy_expert(outputquery, f)
       cur.close()
       conn.close()


   def generate_report_referral(self):
       """Metoda tworzy raport skierowan"""
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
           db_name, db_host, db_un.get(), db_pw.get()),
           cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()

       args=(self.date_from_txt.get(), self.date_to_txt.get())
       sql = """SELECT
kod_icd10 "icd10 / miesiac"
,extract(month FROM data_skierowania) AS miesiac
, count(1) as ilosc
FROM
przychodnia.Skierowania
WHERE
data_skierowania BETWEEN '%s'
AND '%s'
GROUP BY
1
, 2
ORDER BY
1
"""%args
        
       outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(sql)

       with open('raport_skierowania.csv', 'w') as f:
                cur.copy_expert(outputquery, f)
       cur.close()
       conn.close()


   def generate_report_visits(self):
       conn = psycopg2.connect("dbname={} host={} user={} password={}".format(
           db_name, db_host, db_un.get(), db_pw.get()),
           cursor_factory=psycopg2.extras.RealDictCursor)
       cur = conn.cursor()

       args=(self.date_from_txt.get(), self.date_to_txt.get())
       sql = """SELECT
nazwa_doktora "nazwa_doktora / miesiac"
,extract(month FROM data_wizyty) AS miesiac
, count(1) as ilosc
FROM
przychodnia.Wizyty w join przychodnia.Doktorzy d on w.doktor_id=d.doktor_id
WHERE
data_wizyty BETWEEN '%s'
AND '%s'
GROUP BY
1
, 2
ORDER BY
1
"""%args
        
       outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(sql)

       with open('raport_wizyty.csv', 'w') as f:
                cur.copy_expert(outputquery, f)
       cur.close()
       conn.close()


