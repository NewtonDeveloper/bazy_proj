CREATE DATABASE project;
\connect project;

CREATE SCHEMA przychodnia;

CREATE SEQUENCE przychodnia.leki_lek_id_seq;

CREATE TABLE przychodnia.Leki (
                lek_id INTEGER NOT NULL DEFAULT nextval('przychodnia.leki_lek_id_seq'),
                nazwa_leku VARCHAR NOT NULL,
                pozostale VARCHAR,
                CONSTRAINT leki_pk PRIMARY KEY (lek_id)
);


ALTER SEQUENCE przychodnia.leki_lek_id_seq OWNED BY przychodnia.Leki.lek_id;

CREATE SEQUENCE przychodnia.procedury_medyczne_procedura_id_seq;

CREATE TABLE przychodnia.Procedury_Medyczne (
                procedura_id INTEGER NOT NULL DEFAULT nextval('przychodnia.procedury_medyczne_procedura_id_seq'),
                nazwa_procedury VARCHAR NOT NULL,
                pozostale VARCHAR,
                CONSTRAINT procedury_medyczne_pk PRIMARY KEY (procedura_id)
);


ALTER SEQUENCE przychodnia.procedury_medyczne_procedura_id_seq OWNED BY przychodnia.Procedury_Medyczne.procedura_id;

CREATE SEQUENCE przychodnia.skierowania_skierowanie_id_seq;

CREATE TABLE przychodnia.Skierowania (
                skierowanie_id INTEGER NOT NULL DEFAULT nextval('przychodnia.skierowania_skierowanie_id_seq'),
                kod_icd10 VARCHAR,
                diagnoza VARCHAR NOT NULL,
                data_skierowania DATE NOT NULL,
                CONSTRAINT skierowania_pk PRIMARY KEY (skierowanie_id)
);


ALTER SEQUENCE przychodnia.skierowania_skierowanie_id_seq OWNED BY przychodnia.Skierowania.skierowanie_id;

CREATE SEQUENCE przychodnia.adresy_adres_id_seq;

CREATE TABLE przychodnia.Adresy (
                adres_id INTEGER NOT NULL DEFAULT nextval('przychodnia.adresy_adres_id_seq'),
                numer_domu VARCHAR NOT NULL,
                ulica VARCHAR NOT NULL,
                miejscowosc VARCHAR NOT NULL,
                kod_pocztowy VARCHAR NOT NULL,
                CONSTRAINT adresy_pk PRIMARY KEY (adres_id)
);


ALTER SEQUENCE przychodnia.adresy_adres_id_seq OWNED BY przychodnia.Adresy.adres_id;

CREATE SEQUENCE przychodnia.doktorzy_doktor_id_seq;

CREATE TABLE przychodnia.Doktorzy (
                doktor_id INTEGER NOT NULL DEFAULT nextval('przychodnia.doktorzy_doktor_id_seq'),
                adres_id INTEGER NOT NULL,
                nazwa_doktora VARCHAR NOT NULL,
                telefon_doktora VARCHAR NOT NULL,
                pozostale VARCHAR,
                CONSTRAINT doktorzy_pk PRIMARY KEY (doktor_id)
);


ALTER SEQUENCE przychodnia.doktorzy_doktor_id_seq OWNED BY przychodnia.Doktorzy.doktor_id;

CREATE SEQUENCE przychodnia.recepcjonisci_recepcionista_id_seq;

CREATE TABLE przychodnia.Recepcjonisci (
                recepcjonista_id INTEGER NOT NULL DEFAULT nextval('przychodnia.recepcjonisci_recepcionista_id_seq'),
                adres_id INTEGER NOT NULL,
                nazwa_recepcjonisty VARCHAR NOT NULL,
                telefon_recepcjonisty VARCHAR NOT NULL,
                CONSTRAINT recepcjonisci_pk PRIMARY KEY (recepcjonista_id)
);


ALTER SEQUENCE przychodnia.recepcjonisci_recepcionista_id_seq OWNED BY przychodnia.Recepcjonisci.recepcjonista_id;

CREATE SEQUENCE przychodnia.autoryzacja_id_uzytkownik_seq;

CREATE TABLE przychodnia.Autoryzacja (
                uzytkownik_id INTEGER NOT NULL DEFAULT nextval('przychodnia.autoryzacja_id_uzytkownik_seq'),
                recepcjonista_id INTEGER,
                doktor_id INTEGER,
                uzytkownik VARCHAR NOT NULL,
                haslo VARCHAR NOT NULL,
                CONSTRAINT autoryzacja_pk PRIMARY KEY (uzytkownik_id)
);


ALTER SEQUENCE przychodnia.autoryzacja_id_uzytkownik_seq OWNED BY przychodnia.Autoryzacja.uzytkownik_id;

CREATE SEQUENCE przychodnia.pacjenci_pacjent_id_seq;

CREATE TABLE przychodnia.Pacjenci (
                pacjent_id INTEGER NOT NULL DEFAULT nextval('przychodnia.pacjenci_pacjent_id_seq'),
                adres_id INTEGER NOT NULL,
                imie VARCHAR NOT NULL,
                nazwisko VARCHAR NOT NULL,
                plec VARCHAR NOT NULL,
                pesel BIGINT NOT NULL,
                data_narodzin DATE NOT NULL,
                data_stania_sie_pacjentem DATE,
                telefon VARCHAR NOT NULL,
                CONSTRAINT pacjenci_pk PRIMARY KEY (pacjent_id)
);


ALTER SEQUENCE przychodnia.pacjenci_pacjent_id_seq OWNED BY przychodnia.Pacjenci.pacjent_id;

CREATE SEQUENCE przychodnia.wizyty_wizyta_id_seq;

CREATE TABLE przychodnia.Wizyty (
                wizyta_id INTEGER NOT NULL DEFAULT nextval('przychodnia.wizyty_wizyta_id_seq'),
                pacjent_id INTEGER NOT NULL,
                doktor_id INTEGER NOT NULL,
                skierowanie_id INTEGER NOT NULL,
                data_wizyty DATE NOT NULL,
                poczatek TIME NOT NULL,
                koniec TIME NOT NULL,
                calkowity_koszt_wizyty REAL,
                pozostale VARCHAR,
                CONSTRAINT wizyty_pk PRIMARY KEY (wizyta_id)
);


ALTER SEQUENCE przychodnia.wizyty_wizyta_id_seq OWNED BY przychodnia.Wizyty.wizyta_id;

CREATE TABLE przychodnia.Wizyta_Procedura (
                wizyta_id INTEGER NOT NULL,
                procedura_id INTEGER NOT NULL,
                ilosc INTEGER NOT NULL,
                CONSTRAINT wizyta_procedura_pk PRIMARY KEY (wizyta_id, procedura_id)
);


CREATE TABLE przychodnia.Wizyta_Recepta (
                wizyta_id INTEGER NOT NULL,
                lek_id INTEGER NOT NULL,
                ilosc INTEGER NOT NULL,
                CONSTRAINT wizyta_recepta_pk PRIMARY KEY (wizyta_id, lek_id)
);


ALTER TABLE przychodnia.Wizyta_Recepta ADD CONSTRAINT medications_visit_medications_fk
FOREIGN KEY (lek_id)
REFERENCES przychodnia.Leki (lek_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Wizyta_Procedura ADD CONSTRAINT medical_procedures_visit_procedures_fk
FOREIGN KEY (procedura_id)
REFERENCES przychodnia.Procedury_Medyczne (procedura_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Wizyty ADD CONSTRAINT referrals_visits_fk
FOREIGN KEY (skierowanie_id)
REFERENCES przychodnia.Skierowania (skierowanie_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Pacjenci ADD CONSTRAINT addresses_patients_fk
FOREIGN KEY (adres_id)
REFERENCES przychodnia.Adresy (adres_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Recepcjonisci ADD CONSTRAINT addresses_receptionists_fk
FOREIGN KEY (adres_id)
REFERENCES przychodnia.Adresy (adres_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Doktorzy ADD CONSTRAINT addresses_doctors_fk
FOREIGN KEY (adres_id)
REFERENCES przychodnia.Adresy (adres_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Wizyty ADD CONSTRAINT doctors_visits_fk
FOREIGN KEY (doktor_id)
REFERENCES przychodnia.Doktorzy (doktor_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Autoryzacja ADD CONSTRAINT doctors_authorizations_fk
FOREIGN KEY (doktor_id)
REFERENCES przychodnia.Doktorzy (doktor_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Autoryzacja ADD CONSTRAINT receptionists_authorizations_fk
FOREIGN KEY (recepcjonista_id)
REFERENCES przychodnia.Recepcjonisci (recepcjonista_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Wizyty ADD CONSTRAINT patients_visits_fk
FOREIGN KEY (pacjent_id)
REFERENCES przychodnia.Pacjenci (pacjent_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Wizyta_Recepta ADD CONSTRAINT visits_visit_medications_fk
FOREIGN KEY (wizyta_id)
REFERENCES przychodnia.Wizyty (wizyta_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE przychodnia.Wizyta_Procedura ADD CONSTRAINT visits_visit_procedures_fk
FOREIGN KEY (wizyta_id)
REFERENCES przychodnia.Wizyty (wizyta_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
