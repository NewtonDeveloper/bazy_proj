-- stworzenie roli
create role dba with superuser createdb inherit createrole nologin replication bypassrls;
create role app with nosuperuser inherit nocreatedb nocreaterole nologin noreplication nobypassrls;

-- sekwencje dostep
grant all on all sequences in schema przychodnia to dba;
grant all on all sequences in schema przychodnia to app;

-- stworzenie podstawowe role bazy
create user admin login superuser createdb createrole encrypted password 'a123' in role dba;
set role admin;

create role recepcjonista in role app;
create role lekarz in role app;
create role nikt;

-- obostrzenia dla tabeli
ALTER TABLE przychodnia.autoryzacja ADD CONSTRAINT unikalny_uzytkownik UNIQUE (uzytkownik);
ALTER TABLE przychodnia.adresy ADD CONSTRAINT unikalny_adres UNIQUE (numer_domu, ulica, miejscowosc, kod_pocztowy);
ALTER TABLE przychodnia.pacjenci ADD CONSTRAINT unikalny_pacjent UNIQUE (pesel);
ALTER TABLE przychodnia.pacjenci ADD CONSTRAINT plec_sprawdz CHECK (
 plec = 'K'
 OR plec = 'M'
);
ALTER TABLE przychodnia.pacjenci ADD CONSTRAINT pesel_sprawdz CHECK (
    LENGTH(cast (pesel as varchar)) = 11
);
ALTER TABLE przychodnia.pacjenci ADD CONSTRAINT data_sprawdz CHECK (
    data_narodzin <= data_stania_sie_pacjentem
);
ALTER TABLE przychodnia.autoryzacja ADD CONSTRAINT lekarz_albo_recepcjonista_sprawdz CHECK (
    recepcjonista_id IS NULL
    OR doktor_id IS NULL
);

-- stworzenie widoków
create view przychodnia.recepcjonista_pokaz_wizyty as select pacjent_id, doktor_id, skierowanie_id, data_wizyty, poczatek, koniec, pozostale from przychodnia.Wizyty;


-- mozliowosc zalogowania
grant connect on database project to nikt;
grant connect on database project to admin;
grant connect on database project to recepcjonista;
grant connect on database project to lekarz;




grant all on schema przychodnia to lekarz;
grant all on schema przychodnia to recepcjonista;
grant all on schema przychodnia to nikt;




REVOKE ALL ON ALL TABLES IN SCHEMA przychodnia from lekarz;
REVOKE ALL ON ALL TABLES IN SCHEMA przychodnia from recepcjonista;
REVOKE ALL ON ALL TABLES IN SCHEMA przychodnia from nikt;

-- lekarz nieograniczony dostep poza tabela autoryzacja
grant select on all tables in schema przychodnia to lekarz;
revoke select on table przychodnia.autoryzacja from lekarz;

--dostep do odczytu dla recepcionisty
grant select on all tables in schema przychodnia to recepcjonista;
revoke select on table przychodnia.Procedury_Medyczne from recepcjonista;
revoke select on table przychodnia.Wizyta_Procedura from recepcjonista;
revoke select on table przychodnia.Leki from recepcjonista;
revoke select on table przychodnia.Wizyta_Recepta from recepcjonista;
revoke select on table przychodnia.autoryzacja from recepcjonista;


CREATE OR REPLACE FUNCTION przychodnia.dodaj_adres(arg_numer_domu varchar, arg_ulica varchar, arg_miejscowosc varchar, arg_kod_pocztowy varchar)
  RETURNS void AS
  $BODY$
      BEGIN
        insert into przychodnia.Adresy (numer_domu,ulica,miejscowosc,kod_pocztowy) values (arg_numer_domu, arg_ulica, arg_miejscowosc, arg_kod_pocztowy);

          END;
  $BODY$
  LANGUAGE 'plpgsql' security definer;




CREATE OR REPLACE FUNCTION przychodnia.dodaj_pacjenta(arg_adres_id integer, arg_imie varchar, arg_nazwisko varchar, arg_plec varchar, arg_pesel bigint, arg_data_narodzin date, arg_data_stania_sie_pacjentem date, arg_telefon varchar)
  RETURNS void AS
  $BODY$
      BEGIN
        insert into przychodnia.pacjenci (adres_id , imie, nazwisko, plec, pesel, data_narodzin, data_stania_sie_pacjentem, telefon) values (arg_adres_id, arg_imie, arg_nazwisko, arg_plec, arg_pesel, arg_data_narodzin, arg_data_stania_sie_pacjentem, arg_telefon);


          END;
  $BODY$
  LANGUAGE 'plpgsql' security definer;


CREATE OR REPLACE FUNCTION przychodnia.dodaj_skierowanie(arg_kod_icd10 varchar, arg_diagnoza varchar, arg_data_skierowania date)
  RETURNS void AS
  $BODY$
      BEGIN
            insert into przychodnia.Skierowania (kod_icd10, diagnoza, data_skierowania) values (arg_kod_icd10, arg_diagnoza, arg_data_skierowania);

          END;
  $BODY$
  LANGUAGE 'plpgsql' security definer;




CREATE OR REPLACE FUNCTION przychodnia.dodaj_wizyte(arg_pacjent_id integer, arg_doktor_id integer, arg_skierowanie_id integer, arg_data_wizyty date, arg_poczatek time, arg_koniec time, arg_pozostale varchar)
  RETURNS void AS
  $BODY$
      BEGIN

        insert into przychodnia.Wizyty (pacjent_id, doktor_id, skierowanie_id, data_wizyty, poczatek, koniec, pozostale) values (arg_pacjent_id, arg_doktor_id, arg_skierowanie_id, arg_data_wizyty, arg_poczatek, arg_koniec, arg_pozostale);

          END;
  $BODY$
  LANGUAGE 'plpgsql' security definer;


CREATE OR REPLACE FUNCTION przychodnia.dodaj_procedure(arg_wizyta_id integer, arg_procedura_id integer, arg_ilosc integer)
  RETURNS void AS
  $BODY$
      BEGIN

        insert into przychodnia.Wizyta_Procedura(wizyta_id, procedura_id, ilosc) values (arg_wizyta_id,arg_procedura_id, arg_ilosc);

          END;
  $BODY$
  LANGUAGE 'plpgsql' security definer;



CREATE OR REPLACE FUNCTION przychodnia.dodaj_recepte(arg_wizyta_id integer, arg_lek_id integer, arg_ilosc integer)
  RETURNS void AS
  $BODY$
      BEGIN

        insert into przychodnia.Wizyta_Recepta (wizyta_id, lek_id, ilosc) values (arg_wizyta_id, arg_lek_id, arg_ilosc);

          END;
  $BODY$
  LANGUAGE 'plpgsql' security definer;


CREATE OR REPLACE FUNCTION przychodnia.dodaj_lekarza(arg_doktor_id integer, arg_uzytkownik varchar, arg_haslo varchar)
  RETURNS void AS
  $BODY$
      BEGIN
        insert into przychodnia.Autoryzacja (doktor_id, uzytkownik, haslo) values (arg_doktor_id, arg_uzytkownik, arg_haslo);
        EXECUTE 'create user '||arg_uzytkownik||' encrypted password '''||arg_haslo||''' in role lekarz'; 
      END;
  $BODY$
  LANGUAGE 'plpgsql' security definer;


CREATE OR REPLACE FUNCTION przychodnia.dodaj_recepcjoniste(arg_recepcjonista_id integer, arg_uzytkownik varchar, arg_haslo varchar)
  RETURNS void AS
  $BODY$
      BEGIN

        insert into przychodnia.Autoryzacja (recepcjonista_id, uzytkownik, haslo) values (arg_recepcjonista_id, arg_uzytkownik, arg_haslo);
        EXECUTE 'create user '||arg_uzytkownik||' encrypted password '''||arg_haslo||''' in role recepcjonista'; 
    END;
 $BODY$
  LANGUAGE 'plpgsql' security definer;



CREATE OR REPLACE FUNCTION przychodnia.unikalny_czas_wizyty()
 RETURNS trigger AS 
  $BODY$
    DECLARE row record;
      BEGIN 
        FOR row IN SELECT * FROM przychodnia.wizyty
    LOOP
        RAISE NOTICE 'Row is %',row;
        IF row.data_wizyty = new.data_wizyty
        AND row.doktor_id = new.doktor_id
        THEN
            IF new.poczatek >= row.poczatek 
            AND new.poczatek <= row.koniec THEN
                RAISE EXCEPTION 'BLAD – Konflikt wizyt !'; 
            END IF;
            
            IF new.koniec <= row.koniec
            AND new.koniec >= row.poczatek THEN
                RAISE EXCEPTION 'BLAD – Konflikt wizyt !'; 
            END IF;
        END IF;
    END LOOP;
          RETURN NEW; 
          END;
  $BODY$
  LANGUAGE 'plpgsql' security definer;

CREATE TRIGGER kontrola_czasu BEFORE INSERT OR UPDATE ON przychodnia.wizyty
FOR EACH ROW EXECUTE PROCEDURE przychodnia.unikalny_czas_wizyty();


--dostep uzytkownikow do wykonywanie funkcji
grant execute on function przychodnia.dodaj_adres(arg_numer_domu varchar, arg_ulica varchar, arg_miejscowosc varchar, arg_kod_pocztowy varchar) to lekarz;
grant execute on function przychodnia.dodaj_adres(arg_numer_domu varchar, arg_ulica varchar, arg_miejscowosc varchar, arg_kod_pocztowy varchar) to recepcjonista;
grant execute on function przychodnia.dodaj_pacjenta(arg_adres_id integer, arg_imie varchar, arg_nazwisko varchar, arg_plec varchar, arg_pesel bigint, arg_data_narodzin date, arg_data_stania_sie_pacjentem date, arg_telefon varchar) to lekarz;
grant execute on function przychodnia.dodaj_pacjenta(arg_adres_id integer, arg_imie varchar, arg_nazwisko varchar, arg_plec varchar, arg_pesel bigint, arg_data_narodzin date, arg_data_stania_sie_pacjentem date, arg_telefon varchar) to recepcjonista;
grant execute on function przychodnia.dodaj_skierowanie(arg_kod_icd10 varchar, arg_diagnoza varchar, arg_data_skierowania date) to lekarz;
grant execute on function przychodnia.dodaj_skierowanie(arg_kod_icd10 varchar, arg_diagnoza varchar, arg_data_skierowania date) to recepcjonista;
grant execute on function przychodnia.dodaj_wizyte(arg_pacjent_id integer, arg_doktor_id integer, arg_skierowanie_id integer, arg_data_wizyty date, arg_poczatek time, arg_koniec time, arg_pozostale varchar) to lekarz; 
grant execute on function przychodnia.dodaj_wizyte(arg_pacjent_id integer, arg_doktor_id integer, arg_skierowanie_id integer, arg_data_wizyty date, arg_poczatek time, arg_koniec time, arg_pozostale varchar) to recepcjonista; 
grant execute on function przychodnia.dodaj_procedure(arg_wizyta_id integer, arg_procedura_id integer, arg_ilosc integer) to lekarz;
grant execute on function przychodnia.dodaj_recepte(arg_wizyta_id integer, arg_lek_id integer, arg_ilosc integer) to lekarz;


-- dzieki temu dziala grant execute on funtion
REVOKE EXECUTE ON all functions in schema przychodnia FROM public;

