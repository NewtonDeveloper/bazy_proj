--skrypt insert.sql wykonuje administrator po to żeby zainicjować
--bazę jakimiś danymi. Obowiązkiem administratora jest tworzenie
--rekordów tabeli Recepcjonisci i Lekarze oraz nadawanie im loginu i hasła.
-- Dodatkowym obowiązkiem administatora jest dodanie zestawu procedur
-- medycznych oraz leków.

insert into przychodnia.Adresy (numer_domu,ulica,miejscowosc,kod_pocztowy) values ('27','3 Maja Al.', 'Krakow', '30-063');
insert into przychodnia.Adresy (numer_domu,ulica,miejscowosc,kod_pocztowy) values ('41','Hofmana Vlastimila', 'Krakow', '30-210');
insert into przychodnia.Adresy (numer_domu,ulica,miejscowosc,kod_pocztowy) values ('55','Mozarta Wolfganga Amadeusza', 'Krakow', '31-232');
insert into przychodnia.Adresy (numer_domu,ulica,miejscowosc,kod_pocztowy) values ('70','Slowicza', 'Krakow', '31-320');
insert into przychodnia.Adresy (numer_domu,ulica,miejscowosc,kod_pocztowy) values ('156a','Czarnolaska', 'Krakow', '31-619');
insert into przychodnia.Adresy (numer_domu,ulica,miejscowosc,kod_pocztowy) values ('251a','Szwedzka', 'Krakow', '31-143');
insert into przychodnia.Adresy (numer_domu,ulica,miejscowosc,kod_pocztowy) values ('17a','Bobczynskiego', 'Krakow', '31-213');
--
--
--administrator tworzy uzytkowników i hasla, lekarzy i recepjonistow
--
insert into przychodnia.Doktorzy (adres_id, nazwa_doktora, telefon_doktora, pozostale) values (4, 'Gregory House', '824211511', 'Chirurg');
select przychodnia.dodaj_lekarza(1,'house','trudnehaslo');

insert into przychodnia.Doktorzy (adres_id, nazwa_doktora, telefon_doktora, pozostale) values (4, 'Barbara Pawlik', '776211511', 'Okulista');
select przychodnia.dodaj_lekarza(1,'pawlikowa','superhaslo');

insert into przychodnia.Doktorzy (adres_id, nazwa_doktora, telefon_doktora, pozostale) values (6, 'Maria Kowalik', '644511511', 'Kardiolog');
select przychodnia.dodaj_lekarza(1,'kowalik64','haslo64');


insert into przychodnia.Recepcjonisci(adres_id, nazwa_recepcjonisty, telefon_recepcjonisty) values (7, 'Anna Kowalska', '744511511');

select przychodnia.dodaj_recepcjoniste(1,'kowalska74','haslo123');
--
--
--
insert into przychodnia.pacjenci (adres_id , imie, nazwisko, plec, pesel, data_narodzin, data_stania_sie_pacjentem, telefon) values (1,'Krystyna','Szewczyk','K',61121810244, '18-12-1961', '21-12-2018', '123594657');

insert into przychodnia.pacjenci (adres_id , imie, nazwisko, plec, pesel, data_narodzin, data_stania_sie_pacjentem, telefon) values (2,'Marcin','Biel','M',97041415133, '14-04-1997', '22-12-2018', '943822153');

insert into przychodnia.pacjenci (adres_id , imie, nazwisko, plec, pesel, data_narodzin, data_stania_sie_pacjentem, telefon) values (3,'Barbara','Marcisz','K',84110811323, '08-11-1984', '21-12-2018', '284399215');

insert into przychodnia.pacjenci (adres_id , imie, nazwisko, plec, pesel, data_narodzin, data_stania_sie_pacjentem, telefon) values (4,'Krzysztof','Smola','M',77011031293, '03-01-1977', '20-12-2018', '346864876');

insert into przychodnia.pacjenci (adres_id , imie, nazwisko, plec, pesel, data_narodzin, data_stania_sie_pacjentem, telefon) values (3,'Gabriela','Dabrowska','K',92072315535, '23-07-1992', '21-12-2018', '23975321468');


insert into przychodnia.Skierowania (kod_icd10, diagnoza, data_skierowania) values ('H46', 'Zapalenie nerwu wzrokowego', '10-12-2018');

insert into przychodnia.Skierowania (kod_icd10, diagnoza, data_skierowania) values ('R44.1', 'Halucynacje wzrokowe', '12-12-2018');

insert into przychodnia.Skierowania (kod_icd10, diagnoza, data_skierowania) values ('H47.6', 'Zaburzenia kory wzrokowej', '11-12-2018');
insert into przychodnia.Skierowania (kod_icd10, diagnoza, data_skierowania) values ('H47.6', 'Uraz nerwu i dróg wzrokowych', '18-12-2018');


insert into przychodnia.Wizyty (pacjent_id, doktor_id, skierowanie_id, data_wizyty, poczatek, koniec, calkowity_koszt_wizyty, pozostale) values (1,1,1,'22-12-2018','8:00','9:00', 0.0, '');

insert into przychodnia.Wizyty (pacjent_id, doktor_id, skierowanie_id, data_wizyty, poczatek, koniec, calkowity_koszt_wizyty, pozostale) values (2,1,2,'22-12-2018','9:01','10:00', 0.0, '');

insert into przychodnia.Wizyty (pacjent_id, doktor_id, skierowanie_id, data_wizyty, poczatek, koniec, calkowity_koszt_wizyty, pozostale) values (3,1,3,'22-12-2018','10:01','11:00', 0.0, '');

insert into przychodnia.Wizyty (pacjent_id, doktor_id, skierowanie_id, data_wizyty, poczatek, koniec, calkowity_koszt_wizyty, pozostale) values (4,1,4,'22-12-2018','11:01','12:00', 0.0, '');


--
-- Administrator tworzy zestaw procedury medyczne i dostepne leki
--
-- procedrury medyczne
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Konsultacja okulistyczna podstawowa', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Badanie dna oka z rozszerzeniem źrenicy, ocena siatkówki, naczyniówki, ciała szklistego', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Badanie okulistyczne dla celów medycyny pracy', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Pomiar ciśnienia wewnątrgałkowego', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Autorefraktometr (komputerowe badanie wzroku)', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Perymetria statyczna - pole widzenia', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Założenie soczewki kontaktowej (bez ceny soczewki)', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Badanie ostrości wzroku z doborem okularów', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Konsultacje kardiologiczne', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('EKG z opisem', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('USG (ECHO serca) z opisem', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Konsultacje chirurgi ogólnej', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Szycie rany czystej', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Opracowanie rany zakażonej', '');
insert into przychodnia.procedury_medyczne (nazwa_procedury, pozostale) values ('Nacięcie krwiaka, ropniaka', '');


--leki
insert into przychodnia.leki(nazwa_leku, pozostale) values ('Prostego Luteina Complex 30szt', '');
insert into przychodnia.leki(nazwa_leku, pozostale) values ('Thealoz Duo, krople do oczu, 10 ml', '');
insert into przychodnia.leki(nazwa_leku, pozostale) values ('VitA-POS, maść oczna z witamina A, sterylna, 5 g', '');
insert into przychodnia.leki(nazwa_leku, pozostale) values ('Hyal-Drop Multi, krople do oczu i soczewek, nawilżające, 10 ml', '');
insert into przychodnia.leki(nazwa_leku, pozostale) values ('Octenisept, płyn na skórę, 50ml', '');
insert into przychodnia.leki(nazwa_leku, pozostale) values ('Sudocrem, krem do pielęgnacji skóry, 125 g','');
insert into przychodnia.leki(nazwa_leku, pozostale) values ('Maxibiotic, maść, 5 g, tuba','');
insert into przychodnia.leki(nazwa_leku, pozostale) values ('Acard, 75 mg, tabletki 60 szt.','');
insert into przychodnia.leki(nazwa_leku, pozostale) values ('Polocard, 75 mg, tabletki 60 szt.','');
insert into przychodnia.leki(nazwa_leku, pozostale) values ('Aspargin, 250 mg, tabletki, 50 szt.','');







--
--
--
 


